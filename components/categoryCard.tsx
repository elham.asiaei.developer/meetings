import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

export default function CategoryCard(props) {
    const {image ,title}=props
    return (
        <Card sx={{ maxWidth: 300 }}>
            <link> </link>
            <CardMedia
                sx={{ height: 140 }}
                image="/static/images/cards/contemplative-reptile.jpg"
                title="green iguana"
            />
            <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                    {title ? title : "Lizard"}
                </Typography>
                {/*<Typography variant="body2" color="text.secondary">*/}
                {/*    Lizards are a widespread group of squamate reptiles, with over 6,000*/}
                {/*    species, ranging across all continents except Antarctica*/}
                {/*</Typography>*/}
            </CardContent>
            <CardActions>
                <Button size="small">edit</Button>
                <Button size="small">delete</Button>
            </CardActions>
        </Card>
    );
}