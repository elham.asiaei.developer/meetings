import Head from "next/head";

function Layout(props) {
    const {children, title} = props
    return (
        <div>
            <Head title={title}></Head>
            <header>header </header>
            <main>{children}</main>
            <footer>footer</footer>
        </div>
    )
}
export default Layout