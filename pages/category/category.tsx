import {Button} from "@mui/material";
import React, {useRef} from "react"
import {log} from "util";

function Product() {
    const titleInputRef = useRef<any>()

    async function addProductHandler(event) {
        event.preventDefault();
        const title = titleInputRef.current.value
        const response = await fetch('/api/product', {
            method: "POST",
            body: JSON.stringify({title}),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        const responseData = response.json()
        console.log("responseData", responseData)
    }

    return (

        <form onSubmit={addProductHandler}>
            <input type="text" placeholder="title" ref={titleInputRef}/>
            <Button type={"submit"}> addfff</Button>
        </form>
    )

}

export default Product