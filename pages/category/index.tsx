import {Button} from "@mui/material";
import React, {useRef} from "react"
import CategoryCard from "@/components/categoryCard";

function Categories() {
    const titleInputRef = useRef<any>()

    async function addProductHandler(event) {
        event.preventDefault();
        const title = titleInputRef.current.value
        const response = await fetch('/api/product', {
            method: "POST",
            body: JSON.stringify({title}),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        const responseData = response.json()
        console.log("responseData", responseData)
    }

    return (
<div>
    <CategoryCard  title="Techmeet"/>
    <CategoryCard  title="Code Reviwe"/>
    <CategoryCard  title="Workshop"/>
</div>
    )

}

export default Categories