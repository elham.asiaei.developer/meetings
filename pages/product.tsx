import {Button} from "@mui/material";
import React, {useRef, useState} from "react"
import {log} from "util";
function Product () {
 const [data ,setData] = useState<any>()
   async function  addProductHandler (){
     const response = await fetch('/api/product' )
       const responseData=await response.json()
        setData(responseData.data)
    }

    return(
  <>
      <Button onClick={()=>addProductHandler()}> show data</Button>
      {data?.map((item,index) => <div key={index}> {item.title}</div>)
      }
  </>
    )

}

// export  function  getStaticProps(){
//
// }
export default Product